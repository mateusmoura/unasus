// Apresenta a resposta do questionário
$('.block-question__option input').on('change', (e) => {
  $('.block-question__answer').hide();
  $(e.target)
    .parents('.block-question__option')
    .find('.block-question__answer')
    .fadeIn();
});

// Apresenta o conteúdo do sidebar
$('.sidebar-collapse .btn-collapse').on('click', (e) => {
  const container = $(e.target).parents('.sidebar-collapse');

  container.find('.sidebar-collapse__content').slideToggle();

  container.toggleClass('sidebar-collapse__open');

  if (container.hasClass('sidebar-collapse__open')) {
    container
      .find('.fa.fa-caret-down')
      .removeClass('fa-caret-down')
      .addClass('fa-caret-up');
  } else {
    container
      .find('.fa.fa-caret-up')
      .removeClass('fa-caret-up')
      .addClass('fa-caret-down');
  }
});

// Trocar de posição randomicamente as opções
$('.block-question__options').randomize('.block-question__option');
